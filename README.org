#+TITLE: isearch-olc

An in-buffer overlay for the current isearch match counts, ~isearch-lazy-count~ but just after the current matches line.

* Enable
This requires both ~isearch-lazy-highlight~ and ~isearch-lazy-count~ to be non-nil. Then simply enable ~isearch-olc-mode~.

For example:
#+begin_src emacs-lisp
  (add-to-list 'load-path "/path/to/isearch-olc")
  (require 'isearch-olc)

  (custom-set-variables
   '(isearch-olc-format " [%s/%s]"))

  (isearch-olc-mode)
#+end_src

* Configuration
The package currently only exposes one option: ~isearch-olc-format~ which is used to format the result. It is expected to contain two =%s= sequences, the first is replaced with the match number and second the total number of matches.
